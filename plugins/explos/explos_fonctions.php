<?php

//
// #SET_EXPLOS_PARAMS{nom de la page, nom du module (opt), id du secteur du module (opt)} 
// Affecte des variables (SET) au squelette :
// - module, secteur_module et rep
function balise_SET_EXPLOS_PARAMS_dist($p){
	$_code = array();

	for($n=1;$n<3;$n++) {
		$v = interprete_argument_balise($n,$p);
		$_code[] = $v ? $v : '';
	}
	
	// on sauve pour pouvoir recuperer ensuite par #GET
	$p->code = 'vide($Pile["vars"]=array_merge(is_array($Pile["vars"])?$Pile["vars"]:array(),calculer_balise_SET_EXPLOS_PARAMS('. join(', ',$_code).')))';

	$p->interdire_scripts = false; // la balise ne renvoie rien
	return $p;
}

function calculer_balise_SET_EXPLOS_PARAMS($skel='sommaire', $module=''){
	$vars = array();
	$defaut = defined('_DIR_PLUGIN_EXPLOS_MODULE_PORTAIL')?'portail':'defaut';
	$explos = unserialize($GLOBALS['meta']['explos']);
	
	if (!($module))
		$module = $defaut;
		
	elseif ($id_secteur = intval($module)) {
		$module = isset($explos['secteurs'][$id_secteur])?$explos['secteurs'][$id_secteur]:$defaut;
	}
	
	if (!$id_secteur) {
		$id_secteur = isset($explos[$module]['secteur'])?$explos[$module]['secteur']:0;
	}
	
	$vars['module'] = $module; 
	$vars['secteur_module'] = $id_secteur; 

	// tester savoir si le fichier de fond existe
	// sinon on prend le fond par defaut
	if (chemin($module.'/'.$skel.'.html'))
		$vars['rep'] = $module;
	else
		$vars['rep'] = 'defaut';

	// on met dans la Pile pour pouvoir recuperer par #GET
	return $vars;
}

// ajoute dans les variables stockees comme avec SET 
// des parametres de couleurs en fonction du nom du 
// module passe en parametre. On puise alors dans la configuration
// pour retrouver la couleur juste
function balise_SET_EXPLOS_COULEURS_dist($p){
	// on sauve pour pouvoir recuperer ensuite par #GET
	if ($module = interprete_argument_balise(1,$p))
		$p->code = 'vide($Pile["vars"]=array_merge(is_array($Pile["vars"])?$Pile["vars"]:array(),calculer_balise_SET_EXPLOS_COULEURS('.$module.')))';
	$p->interdire_scripts = false; // la balise ne renvoie rien
	return $p;
}

function calculer_balise_SET_EXPLOS_COULEURS($module=''){
	$vars = array();
	if (!$module)
		$module = defined('_DIR_PLUGIN_EXPLOS_MODULE_PORTAIL')?'portail':'defaut';
	
	// recuperer les couleurs
	// on se passe de CFG pour performances
	$explos = unserialize($GLOBALS['meta']['explos']);
	$conf = isset($explos[$module])?$explos[$module]:array();
	$vars['couleur_noir'] = $conf['couleur_noir']?$conf['couleur_noir']:'#0a2f3b';
	$vars['couleur_foncee'] = $conf['couleur_foncee']?$conf['couleur_foncee']:'#347498';
	$vars['couleur_claire'] = $conf['couleur_claire']?$conf['couleur_claire']:'#42a7ce';

	// recuperer la couleur apres le traitement d'image pour les egaliser
	include_spip('inc/filtres_images');
	$img = chemin('img/gris_50.png');
	foreach(array('noir','foncee','claire') as $i)
		$vars['image_'.$i] = '#'.image_graver(image_couleur_extraire(image_sepia($img,$vars['couleur_'.$i])));

	// on met dans la Pile pour pouvoir recuperer par #GET
	return $vars;
}

function balise_EXPLOS_COULEUR_LOGO_dist($p){
	if ($module = interprete_argument_balise(1,$p))
		$p->code = 'calculer_balise_EXPLOS_COULEUR_LOGO('.$module.')';
	$p->interdire_scripts = false; // la balise ne renvoie rien
	return $p;
}

function calculer_balise_EXPLOS_COULEUR_LOGO($module){
	$vars = array();
	if (!$module)
		$module = defined('_DIR_PLUGIN_EXPLOS_MODULE_PORTAIL')?'portail':'defaut';
	// recuperer les couleurs
	return lire_config('explos/'.$module.'/couleur_foncee','#347498');
}


?>
