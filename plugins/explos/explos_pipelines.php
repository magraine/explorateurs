<?php


// apres le calcul d'un fond
function explos_recuperer_fond($flux){
	
	// ajout de la liste des documents lus, dans le formulaire ajouter un document
	if ($flux['args']['fond']=='formulaires/ajouter_un_document') {
		$contexte=array();
		if ($objet = $flux['args']['contexte']['objet']) {
			$_id_objet = id_table_objet($objet);
			$contexte[$_id_objet] = $flux['args']['contexte']['id_objet'];
		}
		include_spip('public/assembler');
		$liste = recuperer_fond('defaut/inc-mini-documents',$contexte);	
		$flux['data']['texte'] .= "\n".$liste;
	}
	return $flux;
}

// au chargement d'un formulaire
function explos_formulaire_charger($flux){
	
	// ajout de l'email dans le formulaire ecrire auteur, si celui ci est connu
	if ($flux['args']['form']=='ecrire_auteur') {
		if ($mail = $GLOBALS['visiteur_session']['email'])
			$flux['data']['email_message_auteur'] = $mail;
	}
	return $flux;
}

// au chargement d'un formulaire
function explos_formulaire_traiter($flux){
	
	if ($flux['args']['form']=='login') {
		$cible = $flux['args']['args'][0];
		// Rediriger aussi si self...
		if ($cible AND ($cible==self())) {
			if (!headers_sent() AND !$_GET['var_mode']) {
				include_spip('inc/headers');
				$flux['data']['redirect'] = $cible;
			} else {
				$flux['data']['message_ok'] .= "<a href='$cible'>" .
				  _T('login_par_ici') .
				  "</a>";
			}
		}
	}
	return $flux;
}

?>
