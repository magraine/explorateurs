<?php

function en_ligne_wiki($flux){
	ksort($flux);
	$html = '';
	include_spip('inc/filtres_images');
	foreach($flux as $i) {
		$im_base = chemin($i['icone']);
		$im1 = url_absolue(extraire_attribut(image_alpha($im_base,80),'src'));
		$im2 = url_absolue(chemin($i['icone']));
		$url = (false!==strpos($i['page'], 'http')) ? $i['page'] : generer_url_public($i['page']);
		$alt = attribut_html($i['titre']);
		$html .= "<a href='$url' onMouseOver=\"jQuery(this).find('img').attr('src','$im2')\" onMouseOut=\"jQuery(this).find('img').attr('src','$im1')\" >"
			. "<img src='$im1' alt='$alt' title='$alt' width='40' height='20' />"
			//. $i['titre']
			. "</a>";
	}
	return $html;	
}

?>
