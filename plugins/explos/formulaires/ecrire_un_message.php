<?php

include_spip('public/assembler');

/*
	$_T est un tableau de chaines de traductions.
	on peut lui envoyer : label, message, bouton, ok
	#ARRAY{
		label,Proposer une hypothèse,
		message,Hypothèse,
		bouton,Soumettre l'hypothèse,
		ok,L'hypothèse a été proposée
	}
*/

function formulaires_ecrire_un_message_charger_dist($id_article, $_T=array(), $url_param_retour='') {

	// Tableau des valeurs servant au calcul d'une signature de securite.
	// Elles seront placees en Input Hidden pour que inc/forum_insert
	// recalcule la meme chose et verifie l'identite des resultats.
	// Donc ne pas changer la valeur de ce tableau entre le calcul de
	// la signature et la fabrication des Hidden
	// Faire attention aussi a 0 != ''
	$ids = array();
	foreach (array('id_article') as $o) {
		$ids[$o] = ($x = intval($$o)) ? $x : '';
	}

	// ne pas mettre '', sinon le squelette n'affichera rien.
	$previsu = ' ';

	// au premier appel (pas de Post-var nommee "retour_forum")
	// memoriser eventuellement l'URL de retour pour y revenir apres
	// envoi du message ; aux appels suivants, reconduire la valeur.
	// Initialiser aussi l'auteur
	if ($retour_forum = rawurldecode(_request('retour')))
		$retour_forum =  str_replace('&var_mode=recalcul','',$retour_forum);
	else {
		// par defaut, on veut prendre url_forum(), mais elle ne sera connue
		// qu'en sortie, on inscrit donc une valeur absurde ("!")
		$retour_forum = "!";
		// sauf si on a passe un parametre en argument (exemple : {#SELF})
		if ($url_param_retour)
			$retour_forum = str_replace('&amp;', '&', $url_param_retour);
		$retour_forum = rawurlencode($retour_forum);
	}
	if (_request('retour_forum')){
		include_spip('formulaires/forum');
		$arg = forum_fichier_tmp(join('', $ids));
		
		$securiser_action = charger_fonction('securiser_action', 'inc');
		// on sait que cette fonction est dans le fichier associe
		$hash = calculer_action_auteur("ajout_forum-$arg");

		// si une hypothese a ete postee, on force les nouvelles valeurs
		// d'arguments pour permettre de poster de nouveau
		if (_request('hypothese_postee')){
			set_request('arg',$arg);
			set_request('hash',$hash);
			set_request('texte','');
		}
	}

	// pour la chaine de hidden
	$script='';
	$script_hidden = $script = str_replace('&amp;', '&', $script);
	foreach ($ids as $id => $v)
		$script_hidden = parametre_url($script_hidden, $id, $v, '&');

	return array(
		'editable'=>true,
		'retour_forum' => $retour_forum,
		'texte' => '',
		'_label'=> (isset($_T['label']) && $_T['label']) ? $_T['label'] : _T('explos:ecrire_un_message'),
		'arg' => $arg,
		'hash' => $hash,
		'url' => $script, # pour les variables hidden
		'url_post' => $script_hidden, # pour les variables hidden
		'nobot' => _request('nobot'),
		'id_forum' => $id_forum, // passer id_forum au formulaire pour lui permettre d'afficher a quoi l'internaute repond
		'_sign'=>implode('_',$ids)
	);
}




function formulaires_ecrire_un_message_verifier_dist($id_article, $_T=array(), $url_param_retour='') {
	include_spip('inc/acces');
	include_spip('inc/texte');
	include_spip('inc/forum');
	include_spip('base/abstract_sql');
	spip_connect();

	$erreurs = array();

	if (strlen($texte = _request('texte')) < 10)
		$erreurs['texte'] = _T('forum_attention_dix_caracteres');
	else if (defined('_FORUM_LONGUEUR_MAXI')
	AND _FORUM_LONGUEUR_MAXI > 0
	AND strlen($texte) > _FORUM_LONGUEUR_MAXI)
		$erreurs['texte'] = _T('forum_attention_trop_caracteres',
			array(
				'compte' => strlen($texte),
				'max' => _FORUM_LONGUEUR_MAXI
			));

	if (!count($erreurs) AND !_request('confirmer_previsu_forum')){
	
		$previsu = inclure_previsu_ecrire_un_message($texte, $_T);
		$erreurs['previsu'] = $previsu;
	}

	return $erreurs;
}

function formulaires_ecrire_un_message_traiter_dist($id_article, $_T=array(), $url_param_retour='') {
	include_spip('inc/acces');
	include_spip('inc/texte');
	include_spip('inc/forum');
	include_spip('base/abstract_sql');
	spip_connect();
	//$message = (isset($_T['ok']) && $_T['ok']) ? $_T['ok'] : _T('explos:message_envoye');
	$forum_insert = charger_fonction('forum_insert', 'inc');

	// pouvoir creer un second post
	set_request('hypothese_postee','oui');

	list($redirect,$id_forum) = $forum_insert();
	return array('redirect'=>$redirect,'id_forum'=>$id_forum);

}

// http://doc.spip.org/@inclure_previsu
function inclure_previsu_ecrire_un_message($texte, $_T=array())
{
	$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
	$message = (isset($_T['message']) && $_T['message']) ? $_T['message'] : _T('explos:message');


	// supprimer les <form> de la previsualisation
	// (sinon on ne peut pas faire <cadre>...</cadre> dans les forums)
	return preg_replace("@<(/?)form\b@ism",
			    '<\1div',
		inclure_balise_dynamique(array('formulaires/inc-ecrire_un_message_previsu',
		      0,
		      array(
			'message' => $message,
			'texte' => safehtml(propre($texte)),
			'erreur' => $erreur,
			'bouton' => $bouton
			)
					       ),
					 false));
}
?>
