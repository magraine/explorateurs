<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('formulaires/editer_article');

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_publier_article_charger_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $_T=array(), $config_fonc='articles_edit_config', $row=array(), $hidden=''){
	$contexte = formulaires_editer_objet_charger('article',$id_article,$id_rubrique,$lier_trad,$retour,$config_fonc,$row,$hidden);
	$contexte['_hidden'] = str_replace('editer_article', 'publier_article', $contexte['_hidden']);
	// preciser que le formulaire doit etre securise auteur/action
	$contexte['_action'] = array('publier_article',$id_article);
	
	$label_titre = (isset($_T['label_titre']) && $_T['label_titre']) ? $_T['label_titre'] : _T('explos:label_titre');
	$contexte['_label_titre'] = $label_titre;
	$value_titre = (isset($_T['value_titre']) && $_T['value_titre']) ? $_T['value_titre'] : _T('explos:value_titre');
	$contexte['_value_titre'] = $value_titre;
	$label_texte = (isset($_T['label_texte']) && $_T['label_texte']) ? $_T['label_texte'] : _T('explos:label_texte');
	$contexte['_label_texte'] = $label_texte;
	$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
	$contexte['_bouton'] = $bouton;
	
	return $contexte;
	
}



function formulaires_publier_article_verifier_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $_T=array(), $config_fonc='articles_edit_config', $row=array(), $hidden=''){

	$erreurs = formulaires_editer_objet_verifier('article',$id_article,array('titre','texte'));
	if($erreurs){
		$label_titre = (isset($_T['label_titre']) && $_T['label_titre']) ? $_T['label_titre'] : _T('explos:label_titre');
		$erreurs['_label_titre'] = $label_titre;
		$value_titre = (isset($_T['value_titre']) && $_T['value_titre']) ? $_T['value_titre'] : _T('explos:value_titre');
		$erreurs['_value_titre'] = $value_titre;
		$label_texte = (isset($_T['label_texte']) && $_T['label_texte']) ? $_T['label_texte'] : _T('explos:label_texte');
		$erreurs['_label_texte'] = $label_texte;
		$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
		$erreurs['_bouton'] = $bouton;
	}
	return $erreurs;
}

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_publier_article_traiter_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $_T=array(), $config_fonc='articles_edit_config', $row=array(), $hidden=''){
	return formulaires_publier_article_traiter2('article',$id_article,$id_rubrique,$lier_trad,$retour,$_T,$config_fonc,$row,$hidden);
}


// modifs de inc/editer.php/formulaires_editer_objet_traiter()
function formulaires_publier_article_traiter2($type, $id='new', $id_parent=0, $lier_trad=0, $retour='', $_T=array(),$config_fonc='articles_edit_config', $row=array(), $hidden=''){

	$res = array();
	
	$new = !intval($id); // surcharge
	$action_editer = charger_fonction("editer_$type",'action');
	list($id,$err) = $action_editer();
// surcharge
	// on publie l'article s'il est nouveau
	if (!$err && $new) {	
		$table_objet_sql = table_objet_sql($type);
		$id_table_objet = id_table_objet($type);
		sql_updateq($table_objet_sql, array('statut'=>'publie'), $id_table_objet."=".sql_quote($id));
	}
// \surcharge
	if ($err){
		$res['message_erreur'] = $err;
	}
	elseif ($retour) {
		include_spip('inc/headers');
		$id_table_objet = id_table_objet($type);
		$res['redirect'] = parametre_url($retour,$id_table_objet,$id);
	}
	return $res;
}

?>
