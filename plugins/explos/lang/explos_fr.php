<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'aide_module' => 'Aide sur le module : ',
'aide' => 'Aide',
'ajouter_netvibes' => 'Ajouter &agrave; Netvibes',

// C
'configurer_couleurs' => 'Coloriages',
'couleur_claire' => 'Couleur claire',
'couleur_foncee' => 'Couleur fonc&eacute;e',
'couleur_noire' => 'Couleur noire',

// E
'ecrire_un_message' => 'Ecrire un message',
'ecrire_auteur' => 'Ecrire &agrave; cet auteur',
'editer_ma_frimousse' => 'Editer ma frimousse',
'editer_mon_profil' => 'Editer mon profil',
'enregistrer' => 'Enregistrer',
'enregistrer_et_previsualiser' => 'Enregistrer et pr&eacute;visualiser',
'editer'=>'Editer',
'erreur_404'=>'La science parfois se perd dans la complexit&eacute; !<br />La page que vous avez demand&eacute; a disparu !',
'explorateurs'=>'Explorateurs',

// I
'inscription' => 'Cr&eacute;er un compte',
'inscrire' => 'S\'inscrire',

// L
'login' => 'Login',
'lire_la_suite' => 'Lire la suite',

// M
'message_envoye' => 'Votre message a &eacute;t&eacute; envoy&eacute;',
'message' => 'Message',

// P
'partenaires' => 'Partenaires',
'pas_logue' => 'Vous avez oubli&eacute; de vous connecter ?',


// R
'rubrique_du_module' => 'Rubrique du module',

// S
'soumettre' => 'Soumettre',
'spip' => 'SPIP',

// T
'titre_documents' => 'Documents',
'tableau_de_bord' => 'Tableau de bord',
'tableau_de_bord_de' => 'Tableau de bord de : @nom@',

// V
'value_titre' => 'Nouvel &eacute;l&eacute;ment',
);

?>
