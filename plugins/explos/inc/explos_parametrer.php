<?php


function explos_config_sauver_secteur($module, $id){
	if (!$module) return false;

	$secteurs = lire_config('explos/secteurs', array());

	// on supprime les id qui ont ce module
	$maj = array();
	foreach($secteurs as $n=>$m){
		if ($m !== $module)
			$maj[$n] = $m;
	}
	// on enregistre en plus de explos/module/secteur = $id
	// un champ explos/secteurs/$id = module
	if ($id) {
		$maj[$id] = $module;
	}
	
	ecrire_config('explos/secteurs', $maj);
	return true;
}

?>
