<?php
ini_set('allow_url_fopen', 1);

$feedURL = 0 ;

if( !$feedURL ) { $feedURL = $_GET ? $_GET['feedURL'] : $HTTP_GET_VARS['feedURL']; }
if( $feedURL && preg_match("/^https?:\/\/.+$/",$feedURL) && ( $feedRef = @fopen($feedURL,'rb') ) ) {
	$contents = ''; while( !feof( $feedRef ) ) { $contents .= fread( $feedRef, 8192 ); }
	$contents = preg_replace("/<(\/?)rss/","<$1rssSpoof",$contents);
	header('Content-Type: text/xml');
	header('Content-Length: '.strlen($contents)); //IE doesn't work without it
	print $contents;
} else {
	header('Content-Type: text/xml');
	header('Content-Length: 508');
?><<?php ?>?xml version="1.0" encoding="ISO-8859-1" ?<?php ?>>
<rssSpoof version="0.91">
  <channel>
    <title>Please enter a valid feed URL</title>
    <link>http://www.google.com/search?q=rss</link> 
    <description></description>
    <language></language>
    <item>
      <title>Invalid feed URL detected</title>
      <link>http://www.google.com/search?q=rss</link>
      <description>RSS newsfeeds are served with a URL beginning with http:// or https://</description>
    </item>
  </channel>
</rssSpoof><?
}
?>