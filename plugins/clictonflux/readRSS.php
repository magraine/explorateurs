<?php
// Test :
// http://127.0.0.1/cliclarue/readRSS.php?rssURL=http://127.0.0.1/moralsoul/actu/rss.php&maxRssItems=3

require_once('simplepie/simplepie.inc.php');
require_once('clictonflux_options.php');

//error_reporting(E_ERROR);

define("MAX_SIZE_DESCRIPTION",2000);	// Nb mx of charaters in description

if(!isset($_GET['rssURL'])){
	die("No RSS url given");
}

if(!file_exists("cache")) mkdir("cache",0775);

if ($trace) {
	$ptLogFile = fopen($logFile,"a");
	
    $lockState = flock($ptLogFile , LOCK_EX);
    for ($i=0; ($lockState == false)&&($i<5); ++$i) {
		usleep(50000); // 0,05 sec
		$lockState = flock($ptLogFile,LOCK_EX);
    }
    if ($lockState) {
		fwrite( $ptLogFile , "\n\t rssUrl:".$_GET['rssURL'] );
    } else {
		fwrite( $ptLogFile , "\nErreur flock" );
	}
}


$feed = new SimplePie();
$feed->feed_url( $_GET['rssURL'] );
$feed->cache_location('cache');
$feed->cache_max_minutes(60);


if(isset($_GET['cacheOff']) && $_GET['cacheOff']=='true' ){
	$feed->enable_caching(false);
	if ($trace) fwrite( $ptLogFile , "\nCache Off" );
} else {
	$feed->enable_caching(true);
	if ($trace) fwrite( $ptLogFile , "\nCache On" );
}



$feed->init();

$feed->handle_content_type();

if ($feed->data) {
	//var_dump($feed->data);

	$maxRssItems = $feed->get_item_quantity( $_GET['maxRssItems'] ) ; 	// Maximum number of RSS news to show.

	echo $feed->get_feed_title()."\n";	
	echo $feed->get_feed_link()."\n";	// Link if channel
	echo $maxRssItems."\n";	// Number of news
	
	$outputItems = array();	// Creating new array - this items holds the data we send back to the client
	for( $no=0; $no<$maxRssItems; $no++ ){ 
		$item = $feed->get_item($no) ;

//		$key = $rssItems[$no]["date_timestamp"].(5000 - $no);
		$key = $item->get_date('U').(0) ;
//		$key = ($rssItems[$no]["date_timestamp"] + abs(crc32($rssItems[$no]["link"]))).(5000) ;

		$outputItems[$key] = array(
			"title"=>$item->get_title(),
			"date_timestamp"=>$item->get_date('U'),
			"description"=>substr($item->get_description(),0,MAX_SIZE_DESCRIPTION).'...',
			"link"=>$item->get_permalink());
	}
	//var_dump($outputItems);
	
	ksort($outputItems,SORT_NUMERIC);	// Sorting items from the key
	$outputItems = array_reverse($outputItems);	// Reverse array so that the newest item appear first

	//var_dump($outputItems);

	$countItems = 0;
	
	foreach($outputItems as $key=>$value){	// Output items - looping throught the array $outputItems
	
		if($value["title"] == "") $value["title"] = "Untitled" ;
		else $value["title"] = preg_replace("/[\r\n]/"," ", $value["title"]) ;
		$value["description"] = $value["description"];
		
		echo "-#-";
		echo $value["title"]."##";	// Title
		echo $value["date_timestamp"]."##";	// Date
		echo preg_replace("/[\r\n]/"," ",$value["description"])."##";	// Description
		echo preg_replace("/[\r\n]/"," ",$value["link"])."##";	// Link
		
		$uniqueIdentifier = ($key + abs(crc32($_GET['rssURL']))) ;
		echo $uniqueIdentifier ;	// Clef unique
		
		if ($trace) {
			fwrite( $ptLogFile , "\n\t\t UniqueIdent : ".$uniqueIdentifier." Key : ".$key." timestamp : ".$value["date_timestamp"]." (".date('d/m/y h:i:s A',$value["date_timestamp"]).") Titre : ".$value["title"] );
		}

		$countItems++;
		if($countItems>=$maxRssItems) exit;	
	
	}
} else {
	if ($trace) {
		fwrite( $ptLogFile , "\nERROR readRSS.php SimplePie : ".$_GET['rssURL']." Msg SimplePie : ".$feed->error );
	}
	echo "\nERROR readRSS.php SimplePie : ".$_GET['rssURL']." Msg SimplePie : ".$feed->error ;
}

if ($trace) {
	fflush($ptLogFile);
	flock($ptLogFile,LOCK_UN);
	fclose($ptLogFile);

}

exit;
?>