<?php

require_once('clictonflux_options.php');

if(!isset($_GET['f'])){
	die("No opml file given");
}

if ($trace) {
	if(!file_exists($logDir)) mkdir($logDir,0775);
	if(!file_exists($logFile)) $ptLogFile = fopen($logFile,"w");
	else $ptLogFile = fopen($logFile,"a");
	fwrite( $ptLogFile , "\n\n".$_SERVER['HTTP_HOST']."  ".date('---d/m/y h:i:s A-------------------------------------------') );
}



$nbCats = -1 ;
$nbFeeds = -1 ;
$opml = array();

blogroll($_GET['f']);

if ($trace) {
	fclose( $ptLogFile );
}


function blogroll($file) {
	global $opml, $nbCats;
	if (!parse_opml($file)) {
		if ($trace) {
			fwrite( $ptLogFile , "\nERROR readOPML.php : Bad Parse file : ".$file );
		} else {
			echo "\nERROR readOPML.php : Bad Parse file : ".$file ;
		}
		return false;
	}

	//var_dump($nbCats); echo "\n\n" ;
	//var_dump($opml); echo "\n\n----" ;
	
	$premiere_ligne = true ;
	
	foreach($opml as $key=>$value){	// Output items - looping throught the array $outputItems
		if ( $premiere_ligne ) $premiere_ligne = false ;
		else echo "\n\n";

		//echo sizeof($value);
		echo $value["CAT"] ;	// Title
		for($num=0 ; $num<sizeof($value)-1 ; $num++) {
			echo "#;#" ;
			echo utf8_encode($value[$num]["TEXT"]."##") ;
			echo utf8_encode($value[$num]["XMLURL"]."##") ;
			echo utf8_encode($value[$num]["NBITEMS"]."##") ;
			echo utf8_encode($value[$num]["TITLE"]) ;
		}
	}


}

function _xml_startElement($xp, $element, $attr) {
	global $opml, $nbCats, $nbFeeds;

	if ( isset($attr['TEXT']) ) {
		if ( isset($attr['TYPE']) ) {
			$nbFeeds ++ ;
			$opml[$nbCats][$nbFeeds] = $attr;
		}
		else {
			$nbCats ++ ;
			$nbFeeds=-1;
			$opml[$nbCats]['CAT'] = utf8_encode($attr['TEXT']);
		}
	}
}

function _xml_endElement($xp, $element) {
	return;
}

function parse_opml($file) {

	if (!$fp = @fopen($file, 'rB'))
		return false;

	$xp = xml_parser_create('iso-8859-1');
	xml_set_element_handler($xp, '_xml_startElement', '_xml_endElement');
	
	while ($fdata = fread($fp, 2048)){
		xml_parse($xp, $fdata, feof($fp)) or die(
			sprintf("Erreur XML : %s à la ligne %d\n",
			xml_error_string(xml_get_error_code($xp)),
			xml_get_current_line_number($xp))
			);
	}
	xml_parser_free($xp);
	return true;
}


?>
