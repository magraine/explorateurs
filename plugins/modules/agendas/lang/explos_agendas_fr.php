<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'acces_non_autorise' => 'Acc&egrave;s non autoris&eacute;',
'agendas' => 'Agenda',
'agendas_description' => 'Mus&eacute;es, Aquarium, Centres de culture scientifique et organisateurs d\'expo ou d\'&eacute;v&eacute;nements sont les Armateurs ! Les Armateurs invitent les explorateurs pour de belles d&eacute;couvertes sur des terres connues et inconnues&nbsp;!',
'agendas_explorateurs' => 'Agenda CST',
'agrandir_carte' => 'Agrandir la carte',
'ajouter_armateur' => 'Ajouter un armateur',
'ajouter_equipier' => 'Ajouter un &eacute;quipier',
'armateurs' => 'Mus&eacute;es',
'aucun_resultat_periode' => 'Aucun r&eacute;sultat pour cette p&eacute;riode',

// C
'configurer_agendas' => 'Configuration de l\'agenda',

// E
'evenements' => 'Ev&eacute;nements',
'erreur_commune' => 'Nom de commune obligatoire',
'explos_agendas' => 'Module Agenda',

// L
'lieu' => 'Lieu',
'liste_equipiers' => 'Liste des &eacute;quipiers',
'logo_armateur' => 'Logo ou photo de l\'armateur',

// N
'nom_ville_code_postal' => 'Nom de la ville ou code postal',

// R
'reduire_carte' => 'R&eacute;duire la carte',

// S
'soumettre_un_evenement' => 'Soumettre un &eacute;v&eacute;nement',
'soumettre_un_evenement_pour' => 'Soumettre un &eacute;v&eacute;nement pour :',
'syndiquer_agendas' => 'Syndiquer cet agenda',

// T
'titre_module' => 'Agenda',
'titre_news_semaine' => 'Les news de la semaine',
'titre_formulaire_recherche' => 'Rechercher un événement',


);

?>
