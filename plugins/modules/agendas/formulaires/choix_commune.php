<?php
/**
 * Plugin Agenda pour Spip 2.0
 * Licence GPL
 * 
 *
 */

include_spip('inc/session');
include_spip('inc/securiser_action');

function formulaires_choix_commune_charger_dist($retour){
	
	$valeurs['id_commune'] = '';
	$valeurs['nom_commune'] = '';

	return $valeurs;
}


function formulaires_choix_commune_verifier_dist($retour){
	
	$nom_commune = _request('nom_commune');
	
	// id_commune obligatoire
	if (!$id_commune = _request('id_commune'))
		$erreurs['nom_commune'] = _L('nom de commune inconnu');

	return $erreurs;
}

function formulaires_choix_commune_traiter_dist($retour){

	$res = array();
	$id_commune = _request('id_commune');
	
	include_spip('base/abstract_sql');
	$id_rubrique = lire_config('explos/agendas/rubrique_ponctuels');
	// on vérifie si un article existe pour cette commune dans la rubrique des events ponctuels
	$id_article = sql_getfetsel('id_article','spip_articles','soustitre='.intval($id_commune).' AND id_rubrique='.intval($id_rubrique));
	// l'article existe on redirige en passant l'id_article
	if ($id_article) {
		include_spip('inc/headers');
		$res['redirect'] = parametre_url($retour,'id_article',$id_article);
	} else {
		// l'article n'existe pas on va le créer
		// on récupère les informations de la commune
		$row = sql_fetsel(	'spip_geo_communes.nom, spip_geo_communes.code_postal, spip_geo_communes.latitude, spip_geo_communes.longitude',
							'spip_geo_communes',
							'id_commune='.intval($id_commune));
		$nom = $row['nom'];
		$code_postal = $row['code_postal'];
		$latitude = $row['latitude'];
		$longitude = $row['longitude'];
		
		include_spip('base/abstract_sql');
		include_spip('inc/texte');
		// pour le traitement :
		// 1) on demande un nouvel article
		// 2) on lui donne un titre et un statut et on y colle les informations de la commune
		// 3) on insère les coordonnées de la commune dans la table gis

		// 1
		include_spip('action/editer_article');
		if (!$id_article = insert_article($id_rubrique)){
			return array('editable'=>true,'message_erreur'=>_T('explos_agendas:erreur_ajout_article'));
		}

		// 2
		$statut = 'publie';
		$surtitre = sql_quote($code_postal);
		$titre = $nom;
		$soustitre = sql_quote($id_commune);
		$c = array(
			'titre'=> $titre,
			'statut'=> $statut
		);
		include_spip('inc/modifier');
		revision_article($id_article, $c);
		instituer_article($id_article, $c);
		sql_update(	'spip_articles',
					array('surtitre' => $surtitre, 'soustitre' => $soustitre),
					'id_article=' . sql_quote($id_article)
				);
		
		//3
		sql_insertq("spip_gis",  array("id_article" => $id_article , "lat" => $latitude, "lonx" => $longitude));
		
		include_spip('inc/headers');
		$res['redirect'] = parametre_url($retour,'id_article',$id_article);

	}

	return $res;
}

?>