<?php


include_spip('inc/session');
include_spip('inc/securiser_action');
include_spip('public/assembler');

function formulaires_ajouter_armateur_charger_dist(){

	$valeurs = array(
		'titre'=>'',
		'texte'=>'',
		'id_commune'=>'',
		'nom_commune'=>'',
		'lat'=>'',
		'lonx'=>'',
		'editable'=>true
	);

	// si on a pas de rubrique, on n'edite pas le formulaire !
	if (!$valeurs['id_secteur'] = lire_config('explos/agendas/rubrique_armateurs')){
		$valeurs['editable']=false;
	}
	
	return $valeurs;
}


function formulaires_ajouter_armateur_verifier_dist(){

	$erreurs = array();
	
	if (!$id_commune = _request('id_commune'))
		$erreurs['nom_commune'] = _T('explos_agendas:erreur_commune');
	
	if (!$titre = _request('titre'))
		$erreurs['titre'] = _T('gis:erreur_titre');
	
	if (!$texte = _request('texte'))
		$erreurs['texte'] = _T('gis:erreur_texte');
	
	return $erreurs;
}


function formulaires_ajouter_armateur_traiter_dist(){
	
	$res = array();

	// ajout de l'article et de ses coordonnées

	include_spip('base/abstract_sql');
	include_spip('inc/texte');
	// pour le traitement :
	// 1) on demande un nouvel article
	// 2) on lui donne un titre et un statut et on y colle le texte
	//3) on insère les coordonnées de l'article et le doc en logo

	// 1
	include_spip('action/editer_article');
	$id_rubrique = lire_config('explos/agendas/rubrique_armateurs');
	if (!$id_article = insert_article($id_rubrique)){
		return array('editable'=>true,'message_erreur'=>_T('explos_agendas:erreur_ajout_article'));
	}

	// 2
	$titre = _request('titre');
	$soustitre = _request('id_commune');
	$statut = 'publie';
	$texte = sql_quote(_request('texte'));
	$c = array(
		'titre'=> $titre,
		'soustitre'=> $soustitre,
		'statut'=> $statut
	);
	include_spip('inc/modifier');
	revision_article($id_article, $c);
	instituer_article($id_article, $c);
	sql_update('spip_articles', array('texte' => $texte), 'id_article=' . sql_quote($id_article));
	
	
	//3
	$lat = _request('lat');
	$lonx = _request('lonx');
	sql_insertq("spip_gis",  array("id_article" => $id_article , "lat" => $lat, "lonx" => $lonx));
	
	// ajout des documents
	// compatibilite php < 4.1
	if (!$_FILES) $_FILES = $GLOBALS['HTTP_POST_FILES'];
	
	if ($fichier = $_FILES['logo_armateur']['name'] != ''){
	
		// recuperation des variables
		$size = $_FILES['logo_armateur']['size'];
		$tmp = $_FILES['logo_armateur']['tmp_name'];
		$type = $_FILES['logo_armateur']['type'];
		$error = $_FILES['logo_armateur']['error'];
		$doc = &$_FILES['logo_armateur'];
		
		// verification si upload OK
		if( !is_uploaded_file($tmp) ) {
			$err = _T('gis:erreur_upload');
		}
		else {
			// on ajoute le doc en logo de l'article
			include_spip('base/abstract_sql');
			include_spip('action/iconifier');
			$spip_image_ajouter = charger_fonction('spip_image_ajouter','action');
			$spip_image_ajouter('arton'.$id_article,true,$doc);
		}
		if ($err){
			$res['message_erreur'] = $err;
		}
	}
	
	include_spip('inc/headers');
	$res['redirect'] = generer_url_entite($id_article, 'article');

	return $res;

}

?>
