<?php
/**
 * Plugin Agenda pour Spip 2.0
 * Licence GPL
 * 
 *
 */
include_spip('inc/actions');
include_spip('inc/editer');
include_spip('inc/autoriser');

function formulaires_ajouter_evenement_charger_dist($id_evenement='new', $id_article=0, $retour='', $lier_trad = 0, $config_fonc='evenements_edit_config', $row=array(), $hidden=''){
	
	$valeurs = formulaires_editer_objet_charger('evenement',$id_evenement,$id_article,0,$retour,$config_fonc,$row,$hidden);
	$valeurs['_hidden'] = str_replace('editer_evenement', 'ajouter_evenement', $contexte['_hidden']);
	// preciser que le formulaire doit etre securise auteur/action
	$valeurs['_action'] = array('ajouter_evenement',$id_evenement);
	
	if (!$valeurs['id_article'])
		$valeurs['id_article'] = $id_article;
	// on récupère la rubrique des armateurs et celle de l'article en cours
	$rubrique_armateurs = lire_config('explos/agendas/rubrique_armateurs');
	$rubrique_article = sql_getfetsel('id_rubrique','spip_articles','id_article='.sql_quote($id_article));
	// si c'est un article de structure et qu'on a pas le droit de modif => paf
	if ($rubrique_article == $rubrique_armateurs AND !autoriser('modifier', 'article', $id_article)){
		$valeurs['editable'] = false;
		$valeurs['message_erreur'] = _T('explos_agendas:acces_non_autorise');
	}
	
	if (!$valeurs['titre_article'])
		$valeurs['titre_article'] = sql_getfetsel('titre','spip_articles','id_article='.intval($valeurs['id_article']));
	// fixer la date par defaut en cas de creation d'evenement
	if (!intval($id_evenement)){
		$t=time();
		$valeurs['date_debut'] = date('Y-m-d H:i:00',$t);
		$valeurs['date_fin'] = date('Y-m-d H:i:00',$t+3600);
		$valeurs['horaire'] = 'oui';
	}
	
	$valeurs['repetitions'] = '';

	// dispatcher date et heure
	list($valeurs["date_debut"],$valeurs["heure_debut"]) = explode(' ',date('d/m/Y H:i',strtotime($valeurs["date_debut"])));
	list($valeurs["date_fin"],$valeurs["heure_fin"]) = explode(' ',date('d/m/Y H:i',strtotime($valeurs["date_fin"])));
	
	// traiter specifiquement l'horaire qui est une checkbox
	if (_request('date_debut') AND !_request('horaire'))
		$valeurs['horaire'] = 'oui';

	return $valeurs;
}

function evenements_edit_config(){
	return array();
}

function formulaires_ajouter_evenement_verifier_dist($id_evenement='new', $id_article=0, $retour='', $lier_trad = 0, $config_fonc='evenements_edit_config', $row=array(), $hidden=''){
	$erreurs = formulaires_editer_objet_verifier('evenement',$id_evenement,array('titre','date_debut','date_fin'));

	include_spip('inc/agenda_gestion');
	
	$horaire = _request('horaire')=='non'?false:true;	
	$date_debut = agenda_verifier_corriger_date_saisie('debut',$horaire,$erreurs);
	$date_fin = agenda_verifier_corriger_date_saisie('fin',$horaire,$erreurs);
	
	if ($date_debut AND $date_fin AND $date_fin<$date_debut)
		$erreurs['date_fin'] = _L('la date de fin doit etre posterieure a la date de debut');

	#if (!count($erreurs))
	#	$erreurs['message_erreur'] = 'ok?';
	return $erreurs;
}

function formulaires_ajouter_evenement_traiter_dist($id_evenement='new', $id_article=0, $retour='', $lier_trad = 0, $config_fonc='evenements_edit_config', $row=array(), $hidden=''){

	$res = array();
	$action_editer = charger_fonction("editer_evenement",'action');
	list($id,$err) = $action_editer();
	if ($err){
		$res['message_erreur'] = $err;
	}
	elseif ($retour) {
		include_spip('inc/headers');
		$retour = parametre_url($retour,'id_evenement',$id);
		if (strpos($retour,'article')!==FALSE){
			$id_article = sql_getfetsel('id_article','spip_evenements','id_evenement='.intval($id));
			$retour = parametre_url($retour,'id_article',$id_article);
		}
		$res['redirect'] = $retour;
	}
	return $res;
}

?>