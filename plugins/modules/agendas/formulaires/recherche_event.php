<?php
/**
 * Plugin Agenda pour Spip 2.0
 * Licence GPL
 * 
 *
 */


function formulaires_recherche_event_charger_dist(){
	
	$valeurs['date_debut'] = date('d/m/Y');
	$valeurs['date_fin'] = date('d/m/Y', strtotime('+1day'));

	return $valeurs;
}


function formulaires_recherche_event_verifier_dist(){

	if (!$date_debut = _request('date_debut'))
		$erreurs['date_debut'] = _L('date debut obligatoire');

	if (!$date_fin = _request('date_fin'))
		$erreurs['date_fin'] = _L('date fin obligatoire');
	// on vérifie et formate les deux dates
	include_spip('inc/agenda_gestion');
	$date_debut = agenda_verifier_corriger_date_saisie('debut',false,$erreurs);
	$date_fin = agenda_verifier_corriger_date_saisie('fin',false,$erreurs);
	// faut pas pousser mémé :p
	if ($date_debut AND $date_fin AND $date_fin<$date_debut)
		$erreurs['date_fin'] = _L('la date de fin doit etre posterieure a la date de debut');

	return $erreurs;
}

function formulaires_recherche_event_traiter_dist(){

	$res = array();
	$date_debut = _request('date_debut');
	$date_fin = _request('date_fin');
	$id_commune = _request('id_commune');
	
	include_spip('inc/agenda_gestion');
	$date_debut = agenda_verifier_corriger_date_saisie('debut',false,$erreurs);
	$date_fin = agenda_verifier_corriger_date_saisie('fin',false,$erreurs);
	
	$date_debut = date('Y-m-d',$date_debut);
	$date_fin = date('Y-m-d',$date_fin);
	
	include_spip('inc/headers');
	$retour = generer_url_public('recherche_event');
	$retour = parametre_url($retour,'date_debut',$date_debut);
	$retour = parametre_url($retour,'date_fin',$date_fin);
	if ($id_commune = _request('id_commune'))
		$retour = parametre_url($retour,'id_commune',$id_commune);
	$res['redirect'] = $retour;


	return $res;
}

?>