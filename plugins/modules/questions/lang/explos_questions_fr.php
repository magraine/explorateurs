<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'au_hasard' => 'Questions au hasard',

// C
'configurer_questions' => 'Configuration de la Bo&icirc;te &agrave; questions',

// D
'detail_question' => 'D&eacute;taillez votre question',
'description_questions' => 'Posez des questions de sciences, des questions de techniques, questions du quotidien, les explorateurs cherchent ensemble des r&eacute;ponses impertinentes !',
'dernieres_questions' => 'Derni&egrave;res questions',

// E
'explos_questions' => 'Module Bo&icirc;te &agrave; questions',

// J
'je_propose_une_reponse' => 'Je propose une r&eacute;ponse',

// L
'boite_questions' => 'Bo&icirc;te &agrave; questions des explorateurs',

// M
'meilleure_reponse' => 'R&eacute;ponse la mieux not&eacute;e',

// N
'nouvelle_question' => 'Nouvelle question',

// Q
'questions' => 'Bo&icirc;te &agrave; questions',
'questions_trouvees' => 'Questions trouv&eacute;es',

// R
'reponse' => 'R&eacute;ponse',
'reponse_soumise' => 'Votre r&eacute;ponse a &eacute;t&eacute; propos&eacute;e&nbsp;!',
'reponses_trouvees' => 'R&eacute;ponses trouv&eacute;es',

// S
'soumettre_reponse' => 'Soumettre la r&eacute;ponse',
'soumettre_question' => 'Soumettre la question',
'syndiquer_questions' => 'Syndiquer les questions',
'syndiquer_reponses_questions' => 'Syndiquer les r&eacute;ponses aux questions',
'syndiquer_reponses_cette_question' => 'Syndiquer les r&eacute;ponses &agrave; cette question',

// T
'titre_question' => 'Votre question',
'titre_module' => 'Bo&icirc;te &agrave; questions',

);

?>
