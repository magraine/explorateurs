<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'carnets_de' => 'Carnets de : @nom@',
'carnets_du_jour' => 'Carnets du jour',
'carnet_des_explorateurs' => 'Le carnet des explorateurs',
'carnets_trouves' => 'Carnets trouv&eacute;s',
'configurer_jaifait' => 'Configuration de j\'ai fait',
'carnets_au_hasard' => 'Carnets au hasard',
'carnets_en_redaction' => 'Mes carnets en cours de r&eacute;daction',

'carnet_propose' => 'Votre carnet vient d\'être propos&eacute; à l\'&eacute;quipe de r&eacute;daction du site.',
'carnet_cree' => 'Votre nouveau carnet vient d\'&ecirc;tre cr&eacute;&eacute;.',
'carnet_enregistre' => 'Le carnet a &eacute;t&eacute; enregistr&eacute;.',

// D
'description_carnet' => 'C\'est l\'espace de publication de vos r&eacute;alisations, de vos projets personnels ou collectifs. <br/> Vous avez invent&eacute;, cr&eacute;&eacute; ou r&eacute;alis&eacute; une machine r&eacute;volutionnaire, publiez-la ici !',

// E
'explos_jaifait' => 'Module J\'ai Fait',
'editer_le_carnet'=>'Editer le Carnet',
'editer_un_carnet'=>'Editer un Carnet',

// J
'jai_fait' => 'J\'ai Fait',

// P
'presente' => 'pr&eacute;sente :',
'presentent' => 'pr&eacute;sentent :',

// R
'reponses_trouvees' => 'Commentaires trouv&eacute;s',

// S
'soumettre_un_carnet' => 'Ecrire un nouveau carnet',
'soumettre_le_carnet' => 'Soumettre le carnet',
'syndiquer_carnets' => 'Syndiquer les carnets',

// T
'titre_carnet' => 'Titre du Carnet',
'texte_carnet' => 'Texte',
'titre_jaifait' => 'Les carnets des explorateurs !',
'categorie_carnet' => 'Cat&eacute;gorie',

);
?>
