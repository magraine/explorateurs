<?php

function explos_module_jaifait_explos_menu_permanent($flux){

	$flux[60] = array(
		'page'=>'jaifait',
		'titre'=>_T('explos_jaifait:jai_fait'),
		'icone'=>'pictos/modules/jaifait.png'
	);
	return $flux;
}
function explos_module_jaifait_explos_menu_sommaire($flux){

	$flux[50] = array(
		'page'=>'jaifait',
		'titre'=>_T('explos_jaifait:jai_fait'),
		'icone'=>'logos/jai-fait-inverse.png'
	);
	return $flux;
}

?>
