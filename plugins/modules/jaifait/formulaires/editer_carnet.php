<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('formulaires/editer_article');

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_editer_carnet_charger_dist($id_article='new', $id_rubrique=0, $retour='', $module='', $statut='', $_T=array()){
	$contexte = formulaires_editer_objet_charger('article',$id_article,$id_rubrique,$lier_trad=0,$retour,$config_fonc='articles_edit_config',$row=array(),$hidden='');
	$contexte['_hidden'] = str_replace('editer_article', 'editer_carnet', $contexte['_hidden']);
	// preciser que le formulaire doit etre securise auteur/action
	$contexte['_action'] = array('editer_carnet',$id_article);
	
	$label_titre = (isset($_T['label_titre']) && $_T['label_titre']) ? $_T['label_titre'] : _T('explos:label_titre');
	$contexte['_label_titre'] = $label_titre;
	$label_texte = (isset($_T['label_texte']) && $_T['label_texte']) ? $_T['label_texte'] : _T('explos:label_texte');
	$contexte['_label_texte'] = $label_texte;
	$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
	$contexte['_bouton'] = $bouton;
	
	// formulaire ou simple previsu ?
	if (_request('editer') or !intval($id_article)) $contexte['edition']=' ';

	return $contexte;
	
}



function formulaires_editer_carnet_verifier_dist($id_article='new', $id_rubrique=0, $retour='', $module='', $statut='', $_T=array()){
	$erreurs = array();
	if (_request('enregistrer'))
		$erreurs = formulaires_editer_objet_verifier('article',$id_article,array('titre','texte'));
	if($erreurs){
		$label_titre = (isset($_T['label_titre']) && $_T['label_titre']) ? $_T['label_titre'] : _T('explos:label_titre');
		$erreurs['_label_titre'] = $label_titre;
		$label_texte = (isset($_T['label_texte']) && $_T['label_texte']) ? $_T['label_texte'] : _T('explos:label_texte');
		$erreurs['_label_texte'] = $label_texte;
		$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
		$erreurs['_bouton'] = $bouton;
	}

	return $erreurs;
}

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_editer_carnet_traiter_dist($id_article='new', $id_rubrique=0, $retour='', $module='', $statut='', $_T=array()){
	$type='article'; 
	$id=$id_article;
	$message = "";
	$res = array();
	
	if (_request('enregistrer')) {
		$new = !intval($id); // surcharge
		$action_editer = charger_fonction("editer_$type",'action');
		list($id,$err) = $action_editer();
	}
	// on change le statut de l'article si bouton 'soumettre' et non 'enregistrer'
	elseif (intval($id) && _request('proposer')) {	
		$table_objet_sql = table_objet_sql($type);
		$id_table_objet = id_table_objet($type);
		sql_updateq($table_objet_sql, array("statut"=>"$statut"), $id_table_objet."=".sql_quote($id));
	}

	if ($err){
		$res = array('message_erreur'=>$err);
	}
	elseif ($retour) {
		include_spip('inc/headers');
		// si soumission, on redirige vers une autre page du module passe en parametre
		// ou vers l'article s'il est publie
		if (_request('proposer')){
			$res = array('message_ok'=>_T('explos_jaifait:carnet_propose'),
					'redirect'=>parametre_url(generer_url_public($module),'var_mode','calcul'));
			if($statut == 'publie')
			$res = array('message_ok'=>_T('explos_jaifait:carnet_propose'),
					'redirect'=>parametre_url(generer_url_public('article','id_article='.$id),'var_mode','calcul'));
		}
		// si c'est un nouveau, il faut transmettre le nouvel id au formulaire pour la previsualisation
		elseif ($new)
			$res = array('message_ok'=>_T('explos_jaifait:carnet_cree'),
					'redirect'=>parametre_url(parametre_url($retour,'carnet',$id),'var_mode','calcul'));
		// sinon, on recharge simplement
		elseif (_request('editer'))
			$res = array('message_ok'=>'', 'editable'=>true);
		else
			$res = array('message_ok'=>_T('explos_jaifait:carnet_enregistre'), 'editable'=>true);
	}

	return array_merge(array('id_article'=>$id),$res);
}



function inclure_previsu_carnet($id_carnet, $_T=array())
{
	$bouton = (isset($_T['bouton']) && $_T['bouton']) ? $_T['bouton'] : _T('explos:soumettre');
	$message = (isset($_T['message']) && $_T['message']) ? $_T['message'] : _T('explos:message');

	// supprimer les <form> de la previsualisation
	// (sinon on ne peut pas faire <cadre>...</cadre> dans les forums)
	return preg_replace("@<(/?)form\b@ism",
			    '<\1div',
		inclure_balise_dynamique(array('formulaires/inc-carnet_previsu',
		      0,
		      array(
			'message' => $message,
			'_bouton' => $bouton,
			'id_article' => $id_carnet,
			)
				),
			false));
}
?>
