<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'configurer_portrait' => 'Configuration de Portrait',

// D
'derniers_portraits' => 'Derniers portraits',
'description_portrait' => 'Beaucoup de gens ont un m&eacute;tier en lien avec les sciences, publiez ici leurs portraits. Les lieux de sciences, industriels, technologiques sont &eacute;galement nombreux, publiez vos reportages ici&nbsp;!',

// E
'explos_portrait' => 'Module Portrait',

// L
'logo_portrait' => 'Logo du portrait',

// P
'portrait' => 'Portraits',
'portraits_au_hasard' => 'Portraits au hasard',
'portraits_en_redaction' => 'Portraits en cours de r&eacute;daction',

// S
'soumettre_le_portrait' => 'Soumettre le portrait',
'soumettre_un_portrait' => 'Soumettre un portrait',
'syndiquer_portraits' => 'Syndiquer les portraits',

// T
'texte_portrait' => 'Texte',
'titre_module' => 'Portraits',
'titre_portrait' => 'Titre du portrait',
'titre_qui_quoi' => 'C\'est qui, c\'est quoi ?',
'titre_voir_ecouter_lire' => 'Voir, &eacute;couter, lire ...',
'titre_ou' => 'C\'est o&ugrave; ?',
'type_portrait' => 'Type de portrait',

);
?>
