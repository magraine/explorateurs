<?php

function explos_module_portrait_explos_menu_permanent($flux){

	$flux[61] = array(
		'page'=>'portrait',
		'titre'=>_T('explos_portrait:portrait'),
		'icone'=>'pictos/modules/portrait.png'
	);
	return $flux;
}
function explos_module_portrait_explos_menu_sommaire($flux){

	$flux[80] = array(
		'page'=>'portrait',
		'titre'=>_T('explos_portrait:portrait'),
		'icone'=>'logos/portrait-inverse.png'
	);
	return $flux;
}

?>
