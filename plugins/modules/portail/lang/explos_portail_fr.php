<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'configurer_portail' => 'Configuration du Portail des Explorateurs',
// E
'explos_portail' => 'Module Portail',
// G
'groupe_mots_licence' => 'Groupe de mots-cl&eacute;s des licences',
// R
'rubrique_mots_doux' => 'Rubrique des mots doux',
'rubrique_presentation' => 'Rubrique de pr&eacute;sentation',

// T
'titre_module' => 'Portail des Explorateurs',

);

?>
