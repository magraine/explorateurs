<?php
function menu_sommaire($flux){
	ksort($flux);
	$html = '';
	include_spip('inc/filtres_images');
	foreach($flux as $i) {
		$img = chemin($i['icone']);
		$couleur = '#ed8213';
		$url = (false!==strpos($i['page'], 'http')) ? $i['page'] : generer_url_public($i['page']);
		$html .= "<li>"
			. "<a href='$url'>"
			. "<img src='" . extraire_attribut(image_graver(image_sepia(image_reduire($img,80,40),$couleur)),'src') . "' alt='" . attribut_html($i['titre']) . "' width='80' height='40' />"
			. $i['titre']
			. "</a>"
			. "</li>\n";
	}
	return "<ul>\n$html\n</ul>\n";
}

function item_list($flux){
	ksort($flux);
	$taille = count($flux);
	$compte = 0;
	$html = '';
	include_spip('inc/filtres_images');
	foreach($flux as $i) {
		$img = chemin($i['icone']);
		$couleur = '#ed8213';
		$url = (false!==strpos($i['page'], 'http')) ? $i['page'] : generer_url_public($i['page']);
		$html .= "{url: '" . $url ."', img: '". extraire_attribut(image_graver(image_sepia(image_reduire($img,80,40),$couleur)),'src') . "', title: '" . texte_script($i['titre']) . "', texte: '" . texte_script($i['titre']) . "'}";
		$compte += 1;
		if ($compte < $taille)
			$html .= ",\n";
	}
	return $html;
}

?>
