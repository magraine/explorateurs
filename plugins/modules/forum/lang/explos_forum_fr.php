<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'configurer_forum' => 'Configuration du Forum',

// D
'derniers_messages' => 'Derniers messages',
'description_forum' => 'Bienvenue sur le forum de discussion des explorateurs.',

// E
'explos_forum' => 'Module Forum',

// T
'titre_forum' => 'Forum',

);

?>
