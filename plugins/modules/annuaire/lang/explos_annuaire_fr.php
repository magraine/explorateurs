<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'annuaire' => 'Carte galactique',

// C
'categories' => 'Cat&eacute;gories',
'configurer_annuaire' => 'Configuration du module Annuaire',

// D
'dernier_site' => 'Dernier site ajout&eacute;',
'derniers_syndics' => 'Nouvelles actus RSS',
'description_annuaire' => 'Pour s\'y retrouver dans l\'univers des sciences et des techniques, rien de tel qu\'une bonne carte pour vous orienter vers des sites Internet&nbsp;! Explorateurs, participez &agrave; la carte en r&eacute;f&eacute;ren&ccedil;ant les sites que vous avez d&eacute;couverts&nbsp;!',

// E
'explos_annuaire' => 'Module Annuaire',

// L
'lire_sur' => 'Lire sur : ',

// P
'proposer_site' => 'Proposer un site',

// S
'sites_au_hasard' => 'Sites au hasard',
'syndiquer_annuaire' => 'Syndiquer l\'annuaire',

// T
'titre_module' => 'Annuaire',

// V
'visiter_le_site' => 'Visiter le site',

);

?>
