<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('formulaires/editer_article');

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_proposer_leondit_charger_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $config_fonc='articles_edit_config', $row=array(), $hidden=''){
	$contexte = formulaires_editer_objet_charger('article',$id_article,$id_rubrique,$lier_trad,$retour,$config_fonc,$row,$hidden);
	$contexte['_hidden'] = str_replace('editer_article', 'proposer_leondit', $contexte['_hidden']);
	// preciser que le formulaire doit etre securise auteur/action
	$contexte['_action'] = array('proposer_leondit',$id_article);
		
	return $contexte;
}



function formulaires_proposer_leondit_verifier_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $config_fonc='articles_edit_config', $row=array(), $hidden=''){

	$erreurs = formulaires_editer_objet_verifier('article',$id_article,array('chapo'));
	
	return $erreurs;
}

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_proposer_leondit_traiter_dist($id_article='new', $id_rubrique=0, $lier_trad=0, $retour='', $config_fonc='articles_edit_config', $row=array(), $hidden=''){
	return formulaires_proposer_leondit_traiter2('article',$id_article,$id_rubrique,$lier_trad,$retour,$config_fonc,$row,$hidden);
}


// modifs de inc/editer.php/formulaires_editer_objet_traiter()
function formulaires_proposer_leondit_traiter2($type, $id='new', $id_parent=0, $lier_trad=0, $retour='', $config_fonc='articles_edit_config', $row=array(), $hidden=''){

	$res = array();
		
	$new = !intval($id); // surcharge
	$action_editer = charger_fonction("editer_$type",'action');
	list($id,$err) = $action_editer();
// surcharge
	// on propose l'article à la publication
	if (!$err && $new) {	
		$table_objet_sql = table_objet_sql($type);
		$id_table_objet = id_table_objet($type);
		sql_updateq($table_objet_sql, array('statut'=>'prop'), $id_table_objet."=".sql_quote($id));
		$res['message_ok'] = _T('explos_leondit:rumeur_proposee');
	}
// \surcharge
	if ($err){
		$res['message_erreur'] = $err;
	}
	elseif ($retour) {
		include_spip('inc/headers');
		$id_table_objet = id_table_objet($type);
		$res['redirect'] = parametre_url($retour,$id_table_objet,$id);
	}
	return $res;
}

?>
