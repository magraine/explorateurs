<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'configurer_leondit' => 'Configuration de L&eacute;on Dit',

// D
'description_leondit' => 'Rumeurs et id&eacute;es re&ccedil;ues nous font penser de travers&nbsp;! Les explorateurs ont l&rsquo;esprit critique&nbsp;! Vous avez un doute sur une id&eacute;e&nbsp;ou des &eacute;claircissements &agrave; apporter&nbsp;: soumettez votre id&eacute;e&nbsp;!',

// E
'explos_leondit' => 'Module L&eacute;on dit',

// I
'info_titre_rumeur' => 'Le titre de la rumeur',
'info_rumeur' => 'La rumeur',
'info_dementi' => 'Le d&eacute;menti',
'info_sources' => 'Les sources pour v&eacute;rifier les infos',

// L
'je_propose_un_dementi' => 'Je propose un d&eacute;menti',
'leondit'=> 'L&eacute;on dit',

// L
'titre_leondit' => 'L&eacute;on dit !',

// M
'meilleure_reponse' => 'Hypothèse la mieux not&eacute;e',

// N
'non_renseigne' => 'Non renseign&eacute;',
'notation_messages' => 'Notation des messages',
'notation_messages_explications' => 'Permettre aux visiteurs de noter les messages de forum ?',
'nouvelle_rumeur' => 'Nouvelle rumeur',

//R
'reponse' => 'D&eacute;menti',
'rumeur_proposee' => 'Votre rumeur a été proposée à la publication, elle sera bientôt publiée sur le site.',
'reponse_soumise' => 'Votre d&eacute;menti a &eacute;t&eacute; propos&eacute;&nbsp;!',
'reponses_proposees' => 'Hypoth&egrave;ses propos&eacute;es',
'reponse_scientifique' => 'Scientifique',

//S
'soumettre_reponse' => 'Soumettre le d&eacute;menti',
'soumettre_une_rumeur' => 'Soumettre une rumeur',
'syndiquer_leondits' => 'Syndiquer les L&eacute;ondits',
'syndiquer_reponses_leondits' => 'Syndiquer les r&eacute;ponses aux L&eacute;ondits',
'syndiquer_reponses_ce_leondit' => 'Syndiquer les r&eacute;ponses de ce L&eacute;ondit',

// T
'titre_module' => 'L&eacute;on Dit',

);

?>
