<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('formulaires/editer_rubrique');

// http://doc.spip.org/@inc_editer_article_dist
function formulaires_ajouter_rubrique_charger_dist($id_rubrique='new', $id_parent=0, $lier_trad=0, $retour='', $config_fonc='rubriques_edit_config', $row=array(), $hidden=''){
	$contexte = formulaires_editer_objet_charger('rubrique',$id_rubrique,$id_parent,$lier_trad,$retour,$config_fonc,$row,$hidden);
	$contexte['_hidden'] = str_replace('editer_rubrique', 'ajouter_rubrique', $contexte['_hidden']);
	// preciser que le formulaire doit etre securise auteur/action
	$contexte['_action'] = array('ajouter_rubrique',$id_rubrique);
	return $contexte;
}


function formulaires_ajouter_rubrique_verifier_dist($id_rubrique='new', $id_parent=0, $lier_trad=0, $retour='', $config_fonc='rubriques_edit_config', $row=array(), $hidden=''){

	$erreurs = formulaires_editer_objet_verifier('rubrique',$id_rubrique,array('titre'));
	return $erreurs;
}

function formulaires_ajouter_rubrique_traiter_dist($id_rubrique='new', $id_parent=0, $lier_trad=0, $retour='', $config_fonc='rubriques_edit_config', $row=array(), $hidden=''){
	$res = array();
	$new = !intval($id); // surcharge
	$type = "rubrique";
	$action_editer = charger_fonction("editer_rubrique",'action');
	list($id,$err) = $action_editer();
// surcharge
	// on publie la rubrique si elle est nouvelle
	if (!$err && $new) {	
		$table_objet_sql = table_objet_sql($type);
		$id_table_objet = id_table_objet($type);
		sql_updateq($table_objet_sql, array('statut'=>'publie'), $id_table_objet."=".sql_quote($id));
		sql_updateq($table_objet_sql, array('date' => 'NOW()'), $id_table_objet."=".sql_quote($id));
	}
// \surcharge
	if ($err){
		$res['message_erreur'] = $err;
	}
	elseif ($retour) {
		include_spip('inc/headers');
		$id_table_objet = id_table_objet($type);
		$res['redirect'] = parametre_url($retour,$id_table_objet,$id);
	}
	return $res;
}
?>
