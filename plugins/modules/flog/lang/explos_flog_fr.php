<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'archives' => 'Archives',
'autueurs_synthese' => 'Auteurs de la synth&egrave;se :',

//D
'description_flog' => 'C\'est l\'espace de publication des carnets de voyage des explorateurs de controverses scientifiques. Ils se sont courageusement confront&eacute;s &agrave; la complexit&eacute; d\'une question scientifique controvers&eacute;e, vous pouvez ici d&eacute;couvrir leur travail. Vous pouvez aussi explorer une controverse !',
'detail_synthese' =>'Texte de la synth&egrave;se',
'discussions' =>'Discussions',

//E
'editer_synthese' => 'Editer',
'explos_flog' => 'Module Flog',
'explication_soumettre_problematique' => 'Une probl&eacute;matique, c\'est la pr&eacute;sentation de la controverse &agrave; explorer. Les membres du groupe produisent des synth&egrave;ses (Ces synth&egrave;ses sont soit des fiches de lecture, synth&egrave;ses de discussions, comptes rendus de r&eacute;unions). Elles sont publi&eacute;es &agrave; la suite de la probl&eacute;matique et peuvent &ecirc;tre comment&eacute;e par le public&nbsp;!! Le groupe dispose d\'un espace de stockage et de classement de l\'information (liens, fichiers divers,...). Cet espace est priv&eacute; et r&eacute;serv&eacute; au groupe. &Agrave; l\'issue des travaux une synth&egrave;se finale est propos&eacute; sur ce site.',
'explication_soumettre_synthese' => 'Une synth&egrave;se, ...', // non utilisé

// F
'flog' => 'FLOG',

// M
'membres_flog' => 'Membres de ce FLOG',

// N
'nouveautes' => 'Les explorations en cours',
'nouvelle_problematique' =>'Nouvelle probl&eacute;matique',
'nouvelle_synthese' =>'Nouvelle synth&egrave;se',

// P
'previsu_synthese' =>'Pr&eacute;visualiser',
'poster_message' =>'Poster un message',

// R
'recherche_messages' =>'R&eacute;sultats dans les forums',
'recherche_problematiques' =>'R&eacute;sultats dans les probl&eacute;matiques',
'recherche_syntheses' =>'R&eacute;sultats dans les synth&egrave;ses',

// S
'soumettre_une_problematique' =>'Soumettre une probl&eacute;matique',
'soumettre_une_synthese' =>'Soumettre une synth&egrave;se',
'syndiquer_sujets' =>'Syndiquer les sujets',
'syndiquer_syntheses' =>'Syndiquer les synth&egrave;ses',
'syndiquer_syntheses_sujet' =>'Syndiquer les synth&egrave;ses de ce sujet',
'syntheses' =>'Synth&egrave;ses',
'syntheses_prepa' =>'Synth&egrave;ses en cours de rédaction',

// T
'titre_module' => 'FLOG',
'titre_synthese' => 'Titre de la synth&egrave;se',
'titre_flog' => 'Le FLOG',
'titre_soumettre_problematique' =>'Ajouter une probl&eacute;matique',
'titre_soumettre_synthese' =>'R&eacute;diger la premi&egrave;re synth&egrave;se',

//V
'voir_archives' => 'Consulter les archives',

);

?>
