<?php

function autoriser_document_voir($faire, $type, $id, $qui, $opt){

	if (!isset($GLOBALS['meta']["creer_htaccess"])
	OR $GLOBALS['meta']["creer_htaccess"] != 'oui') {
		// si article, verifier l'autorisation aussi
		return autorisation_des_documents($id, $opt, $qui);
	}

	if ((!is_numeric($id)) OR $id < 0) return false;

	if (in_array($qui['statut'], array('0minirezo', '1comite')))
		// si article, verifier l'autorisation aussi
		return autorisation_des_documents($opt, $qui,'htaccess');

	if ($liens = sql_allfetsel('objet,id_objet', 'spip_documents_liens', 'id_document='.intval($id)))
	foreach ($liens as $l) {
		$table_sql = table_objet_sql($l['objet']);
		$id_table = id_table_objet($l['objet']);
		if (sql_countsel($table_sql, "$id_table = ". intval($l['id_objet'])
		. (in_array($l['objet'], array('article', 'rubrique', 'breve'))
			? " AND statut = 'publie'"
			: '')
		) > 0)
			// si article, verifier l'autorisation aussi
			return autorisation_des_documents($opt, $qui,'htaccess');
	}
	return false;
}

function autorisation_des_documents($id, $opt, $qui, $val_retour_ok=true){
	// si article, verifier l'autorisation aussi
	if ($opt && isset($opt['objet']) && ($opt['objet'] == 'article')
	&& isset($opt['id_objet']) && $opt['id_objet']
	&& !autoriser('voir','documentsarticles',$opt['id_objet'],$qui, $opt))
			return false;
	
	return $val_retour_ok;	
}

function autoriser_documentsarticles_voir($faire, $type, $id, $qui, $opt){
	// pas de cfg : pas de rubrique definies
	if (!function_exists('lire_config')) {
		return true;
	}
	// verifier qu'on a le droit de voir ce document
	$id_rubrique = sql_getfetsel('id_rubrique', 'spip_articles', "id_article = ".sql_quote($id));
	#spip_log("rub : " . $id_rubrique,'portfolio');
	#spip_log("mys : " . lire_config('explos/mystere/secteur'),'portfolio');

	if ($id_rubrique == lire_config('explos/mystere/secteur')) {
		// admin ou restreint
		if (($qui['statut'] == '0minirezo')
		AND (!$qui['restreint'] OR in_array($id_rubrique, $qui['restreint'])))
			return true;
		// auteur de l'article
		if (sql_countsel('spip_auteurs_articles', array('id_auteur=' . sql_quote($qui['id_auteur']), 'id_article=' . sql_quote($id))))
			return true;

		return false;
	}
	
	return true;
}

?>
