<?php

function explos_module_mystere_explos_menu_permanent($flux){
	$flux[25] = array(
		'page'=>'mystere',
		'titre'=>_T('explos_mystere:photo_mystere'),
		'icone'=>'pictos/modules/mystere.png'
	);
	return $flux;
}

function explos_module_mystere_explos_menu_sommaire($flux){
	$flux[70] = array(
		'page'=>'mystere',
		'titre'=>_T('explos_mystere:mystere'),
		'icone'=>'logos/mystere-inverse.png'
	);
	return $flux;
}

// a la pre_edition d'elements spip.
function explos_module_mystere_pre_edition($flux){
	
	// au moment de publier un article, si c'est une photo mystere,
	// fixer la date de publication a j+1 du dernier 
	// si dernier > aujourd'hui, sinon a aujourd'hui.
	if ($flux['args']['action'] == 'instituer'
	AND $flux['args']['table'] == 'spip_articles'
	AND $flux['data']['statut'] == 'publie'
	) {
		// si le secteur est celui d'un mystere
		$id_secteur = sql_getfetsel('id_secteur','spip_articles','id_article='.sql_quote($flux['args']['id_objet']));
		if ($id_secteur AND $id_secteur == lire_config('explos/mystere/secteur')) {
			// on cherche la date de derniere publication
			$dernier = sql_getfetsel('date','spip_articles',array('id_secteur=' . sql_quote($id_secteur), 'statut=' . sql_quote('publie')),'','date DESC','1');
			$dernier = strtotime($dernier);
			$aujourdhui = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
			// le dernier est vieu, on prend aujourd'hui
			if ($dernier < $aujourdhui) {
				$flux['data']['date'] = date('Y-m-d H:i:s');
			}
			// sinon on prend le suivant
			else {
				$flux['data']['date'] = date('Y-m-d H:i:s', 
					mktime(date("H"), date("i"), date("s"), date("m",$dernier), date("d",$dernier)+1, date("Y",$dernier)));
			}
		}
	}
	return $flux;
}
?>
