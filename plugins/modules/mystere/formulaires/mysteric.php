<?php


include_spip('inc/session');
include_spip('public/assembler');

function formulaires_mysteric_charger_dist(){

	// exiger l'authentification des posteurs
/*
	if (!$GLOBALS["visiteur_session"]['statut']) {
		return array(false,array(
			'login_forum_abo'=>' ',
			'inscription' => generer_url_public('identifiants', 'lang='.$GLOBALS['spip_lang']),
			'oubli' => generer_url_public('spip_pass','lang='.$GLOBALS['spip_lang'],true),
			));
	}
*/

	// si les clefs changent : on efface les images qui ont pu etre envoyees
	// car c'est certainement que le visiteur veut poster un second formulaire ?
	$cle = md5(uniqid(rand()));
	$valeurs = array(
		'texte'=> _T('explos_mystere:texte_defaut'),
		'cle_mystere' => $cle, // nouvelle cle, sauf si le formulaire est poste
		'nouvelle_cle_mystere' => $cle, // nouvelle cle
		'id_mot'=>'',
		'editable'=>true
	);

	// si on a pas de rubrique, on n'edite pas le formulaire !
	if (!$id_rubrique = lire_config('explos/mystere/secteur')){
		return false;
	}
	// l'ajout de photos est-il autorise ?
	// cf. verifier.php
	include_spip('inc/securiser_action');
	$param_photos = array('photo_question', 'photo_reponse');
	foreach ($param_photos as $photo)
		$valeurs['cle_'.$photo] = calculer_cle_action($photo.'-'.$id_rubrique);


	// on recherche l'adresse de photos deja presentes (pour previsu in situ)
	// seulement si on vient de poster... (sinon ce sont des anciennes images)
	if (_request('cle_mystere')) {
		$photos = array();
		foreach ($param_photos as $photo) {
			// stocker des eventuelles photos dans un espace temporaire
			// portant la cle du formulaire ; et ses metadonnees avec
			$tmp_var = 'tmp_mysteric_'.$photo;
			if (!isset($GLOBALS['visiteur_session'][$tmp_var])) {
				session_set($tmp_var,	
				sous_repertoire(_DIR_VAR,'tmp_mysteric').md5(uniqid(rand())));		
			}
			$tmp = $GLOBALS['visiteur_session'][$tmp_var];

			if (file_exists($tmp.'.txt')
				&& lire_fichier($tmp.'.txt', $meta)
				&& ($info = @unserialize($meta)))
			{
				$valeurs['tmp_'.$photo]=$info['tmp_name'];
			}
		}
	} else {
		// si le formulaire n'est pas soumis,
		// on peut se permettre de supprimer les liens vers les
		// anciennes photos
		foreach ($param_photos as $photo)
			session_set('tmp_mysteric_'.$photo);
	}
	
	// si l'on vien juste de poster le formlaire et qu'il a ete valide
	// on veut pouvoir recommencer a poster un nouvelle photo
	// on ne prend du coup pas les anciennes valeurs dans l'environnement
	// pour ne pas polluer le nouveau formulaire
	if (_request('soumission_mysteric_enregistre')){
		unset ($valeurs['texte']);
		unset ($valeurs['id_mot']);
		// et les photos ?
	}
	return $valeurs;
}


function formulaires_mysteric_verifier_dist(){

	$erreurs = array();

	// si pas loggue... on se sauve !
	if (!$GLOBALS['visiteur_session']['id_auteur']){
		$erreurs['message_erreur'] = _T('explos_mystere:identification_obligatoire');
	}
	
	if (!$texte = _request('texte')){
		$erreurs['texte'] = _T('explos_mystere:erreur_essayer_dexpliquer');
	}
	
	if (!$id_mot = _request('id_mot')){
		$erreurs['id_mot'] = _T('explos_mystere:erreur_choix_licence');
	}

	include_spip('base/abstract_sql');
	include_spip('inc/texte');
	
	$id_rubrique = lire_config('explos/mystere/secteur');
	$photos = array();
	foreach (array('photo_question','photo_reponse') as $photo) {
		// stocker des eventuelles photos dans un espace temporaire
		// portant la cle du formulaire ; et ses metadonnees avec
		$tmp_var = 'tmp_mysteric_'.$photo;
		if (!isset($GLOBALS['visiteur_session'][$tmp_var]) || _request('nouvelle_demande')) {
			session_set($tmp_var,		
			sous_repertoire(_DIR_VAR,'tmp_mysteric').md5(uniqid(rand())));		
		}
		$tmp = $GLOBALS['visiteur_session'][$tmp_var];

		$doc = &$_FILES[$photo];
		
		if (isset($_FILES[$photo])
		AND $_FILES[$photo]['tmp_name']) {
			// securite :
			// verifier si on possede la cle (ie on est autorise a poster)
			// (sinon tant pis) ; cf. charger.php pour la definition de la cle
			include_spip('inc/securiser_action');
			if (_request('cle_'.$photo) != calculer_cle_action("$photo-$id_rubrique")) {
				$erreurs[$photo] = _T('explos_mystere:erreur_pas_de_cle');
				unset($_FILES[$photo]);
			} else {
				include_spip('inc/ajouter_documents');
				list($extension,$doc['name']) = fixer_extension_document($doc);
				$acceptes = array_map('trim', explode(',',lire_config('explos/mystere/formats_images','jpg,png,gif')));

				if (!in_array($extension, $acceptes)) {
					if (!$formats = join(', ',$acceptes))
						$formats = _L('aucun');
					$erreurs[$photo] = _T('explos_mystere:erreur_formats_acceptes',array('formats' => $formats));
				}
				else {
					include_spip('inc/getdocument');
					if (!deplacer_fichier_upload($doc['tmp_name'], $tmp.'.'.$extension))
						$erreurs[$photo] = _T('explos_mystere:erreur_copie_impossible');

	#		else if (...)
	#		verifier le type_document autorise
	#		retailler eventuellement les photos
				}

				// si ok on stocke les meta donnees, sinon on efface
				if (isset($erreurs[$photo])) {
					spip_unlink($tmp.'.'.$extension);
					unset ($_FILES[$photo]);
				} else {
					$doc['tmp_name'] = $tmp.'.'.$extension;
					ecrire_fichier($tmp.'.txt', serialize($doc));
				}
			}
		}
		// restaurer le document uploade au tour precedent
		else if (file_exists($tmp.'.txt')
			&& lire_fichier($tmp.'.txt', $meta)
			&& ($info = @unserialize($meta)))
		{
			if (_request('suprimer_'.$photo)) {
				spip_unlink($info['tmp_name']);
				spip_unlink($tmp.'.txt');
			} else {
				$doc = $info;
			}
		}

		// pas de photo (bof bof !)
		else {
			$erreurs[$photo] = _T('explos_mystere:erreur_pas_de_photo');
		}

		$photos[$photo] = $doc;
	}


	// affichage de la previsualisation
	if (!count($erreurs) AND !_request('confirmer_previsu_mysteric')){
		if ($afficher_texte != 'non') {
			$previsu = inclure_previsu_mysteric($texte, $photos);
			$erreurs['previsu'] = $previsu;
		}
	}
	
	return $erreurs;
}


function formulaires_mysteric_traiter_dist(){
	include_spip('base/abstract_sql');
	include_spip('inc/texte');

	// pour le traitement :
	// 1) on demande un nouvel article
	// 2) on lui donne un titre ('Photo mystère n°XX')
	// 2b) on lui attache le mot cle de la licence choisie
	// 3) on lui ajoute son logo (photo question)
	// 4) on lui cree un document (photo reponse) et on met le texte
	// dans le descriptif ce de document.

	// 1
	include_spip('action/editer_article');
	$id_rubrique = lire_config('explos/mystere/secteur');
	if (!$id_article = insert_article($id_rubrique)){
		return array(1,_T('explos_mystere:erreur_ajout_mystere'));
	}

	// 2
	$c = array(
		'titre'=> str_replace('@numero@', $id_article, lire_config('explos/mystere/titre_article')),
		'statut'=>'prop'
	);
	include_spip('inc/modifier');
	revision_article($id_article, $c);
	instituer_article($id_article, $c);
	
	// 2b
	if ($id_mot = intval(_request('id_mot')))
		sql_insertq("spip_mots_articles", array("id_mot" => $id_mot , "id_article" => $id_article));
	
	// 3
	$tmp = $GLOBALS['visiteur_session']['tmp_mysteric_photo_question'];
	lire_fichier($tmp.'.txt', $meta);
	$info = @unserialize($meta);
	include_spip('action/iconifier');
	$spip_image_ajouter = charger_fonction('spip_image_ajouter','action');
	$spip_image_ajouter('arton'.$id_article,true,$info); // true pour dire que l'on indique une source de fichier (donc on prend pas la premiere de $_FILES)
	include_spip('inc/flock');
	spip_unlink($info['tmp_name']);
	spip_unlink($tmp.'.txt');


	// 4
	$tmp = $GLOBALS['visiteur_session']['tmp_mysteric_photo_reponse'];
	lire_fichier($tmp.'.txt', $meta);
	$info = @unserialize($meta);
	$ajouter_documents = charger_fonction('ajouter_documents','inc');
	if (!$id_document = $ajouter_documents($info['tmp_name'], $info['name'], "article", $id_article, 'document', $id_document=0, $docs_actifs=array()))
		return array(1, _T('explos_mystere:erreur_enregistrement_document'));
	$c = array(
		'descriptif'=>_request('texte')
	);
	revision_document($id_document, $c);
	spip_unlink($info['tmp_name']);
	spip_unlink($tmp.'.txt');
	
	
	// signaler que l'on vient de soumettre le formulaire
	// pour que charger ne remette pas les anciennes valeurs
	// puisqu'on propose sa reedition dans la foulee
	set_request('soumission_mysteric_enregistre',true);

	// on fait un cron ici : 
	// supprimer les documents en cache qui sont trop vieux,
	$dir = @opendir($d = _DIR_VAR.'tmp_mysteric');
	$t = time()  - (24 * 3600);
	while(($f = @readdir($dir)) !== false) {
		$f = $d.'/'.$f;
		if (is_file($f) AND ($t > @filemtime($f))) {
			spip_unlink($f);
		}
	}
	
	$lien_auteur = generer_url_entite($GLOBALS['visiteur_session']['id_auteur'], "auteur");
	return array(1,_T("explos_mystere:ok_formulaire_soumis", array("lien_auteur" => $lien_auteur)));
}




// http://doc.spip.org/@inclure_previsu
function inclure_previsu_mysteric($texte, $photos)
{
	$bouton = _T('explos_mystere:bouton_message_definitif');

	$contexte = array();
	foreach ($photos as $photo=>$tmp){
		$contexte[$photo] = $tmp['name'];
		$contexte['tmp_'.$photo] = $tmp['tmp_name'];
	}
	$contexte = array_merge(
		$contexte,
		array(
			'texte' => safehtml(propre($texte)),
			'erreur' => $erreur,
			'bouton' => $bouton
			)
		);
	// supprimer les <form> de la previsualisation
	// (sinon on ne peut pas faire <cadre>...</cadre> dans les forums)
	return preg_replace("@<(/?)form\b@ism",
			'<\1div',
			inclure_balise_dynamique(array('formulaires/mysteric_previsu',0,$contexte),
			false));
}
?>
