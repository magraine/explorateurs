<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'archives' => 'Archives',

// B
'bouton_message_definitif' => 'Confirmer l\'envoi',
'bouton_voir_avant_envoi' => 'Proposer le myst&egrave;re',

// C
'choix_licence' => 'Licence des photos',
'configurer_mysteric' => 'Configuration de Mysteric',
#'ce_secteur' => 'Prendre cette rubrique',
'cacher_solution' => 'Cacher',

// D
'description_mystere' => 'La photo myst&egrave;re ! Chaque semaine une photo myst&egrave;re diff&eacute;rente ! la r&eacute;ponse : 7 jours apr&egrave;s la publication. Explorateurs ! Proposez, vous aussi, vos photos myst&egrave;res ! Objectif : la photo myst&egrave;re quotidienne !',

// E
'explos_mystere' => 'Module Photo Myst&egrave;re',
'erreur_ajout_mystere' => 'D&eacute;sol&eacute;, un probl&egrave;me est survenu &agrave; la cr&eacute;ation de votre myst&egrave;re...',
'erreur_enregistrement_document' => 'D&eacute;sol&eacute;, mais une erreur est survenue &agrave; la copie du document...',
'erreur_choix_licence' => 'Veuillez choisir une licence pour vos photos.',
'erreur_copie_impossible' => 'Impossible de copier le document',
'erreur_essayer_dexpliquer' => 'Essayez d\'expliquer un peu non de non !',
'erreur_pas_de_cle' => 'Il faut trouver une cl&eacute; valide pour envoyer des photos !',
'erreur_pas_de_photo' => 'H&eacute; bonhomme ! Il faut une photo l&agrave; !',
'erreur_formats_acceptes' => 'Formats accept&#233;s : @formats@.',
'explications' => 'Explications',
'explications_previsu' => 'Vous pouvez modifier si vous le souhaitez, sinon validez.',

// H
'hypothese' => 'Hypoth&egrave;se',

// I
'identification_obligatoire' => 'Vous devez &ecirc;tre identifi&eacute;s pour pouvoir poster une photo myst&egrave;re !',
'info_rubrique_cible' => 'Rubrique contenant la photo myst&egrave;re',
'info_devoiler' => 'D&eacute;voiler la r&eacute;ponse après :',
'info_photo_question' => 'Photo de la question',
'info_photo_reponse' => 'Photo de la r&eacute;ponse',
'info_texte_reponse' => 'Texte expliquant la r&eacute;ponse',
'info_formats_images' => 'Images souhait&eacute;es',
'info_stoper_forum' => 'Arr&ecirc;ter les commentaires apr&egrave;s',
'info_titre_article' => 'Titre des articles',
'texte_titre_article' => 'Titre donn&eacute; par d&eacute;faut lors de la cr&eacute;ation d\'article
via le formulaire de soumission de myst&egrave;re. @numero@ sera remplac&eacute; par l\'identifiant de l\'article cr&eacute;&eacute;',
'texte_devoiler_nb_jour' => '(en nombre de jours)',
'texte_formats_images' => 'S&eacute;parez les extensions autoris&eacute;es par des virgules (ex: jpg,png) :', 

// J
'jemets_une_hypothese' => 'J\'emets une hypothèse',

// J
'licence' => 'Licence',

// M
'message_hypothese_soumise' => 'Votre hypoth&egrave;se a &eacute;t&eacute; prise en compte.',
'mystere'=> 'Myst&egrave;re',

// N
'notation_hypotheses' => 'Notation des hypoth&egrave;ses',
'notation_hypotheses_explications' => 'Permettre aux visiteurs de noter les hypoth&egrave;ses ?',

// O
'ok_formulaire_soumis' => 'Votre myst&egrave;re a &eacute;t&eacute; envoy&eacute; sur le site, il sera bient&ocirc;t publi&eacute;.<br /> Vous pouvez aller voir vos mystères en attente sur <a href="@lien_auteur@">votre tableau de bord</a>.',
'ou_choisir' => 'Ou choisir :',

// P
'par_hasard'=>'Au hasard',
'photo_mystere'=>'Photo myst&egrave;re',
'photo_du_jour' => 'Photo du jour',
'photos_precedentes' => 'Photos pr&eacute;c&eacute;dentes',
'photos_suivantes' => 'Photos suivantes',
'poster_commentaire' => 'Un commentaire ?',

// Q
'question' => 'Question',

// R
'racine' => 'Racine',
'reponse' => 'R&eacute;ponse',


// S
'secteur' => 'Secteur :',
'soumettre' => 'Soumettre l\'hypoth&egrave;se',
'soumettre_un_mystere' => 'Soumettre un myst&egrave;re',
'solution' => 'Solution du myst&egrave;re',
'solution_precedentes' => 'Solutions pr&eacute;c&eacute;dentes',
'syndiquer_mysteres' => 'Syndiquer les photos myst&egrave;res',
'syndiquer_reponses_mysteres' => 'Syndiquer les r&eacute;ponses propos&eacute;es',
'syndiquer_reponses_ce_mystere' => 'Syndiquer les r&eacute;ponses de ce myst&egrave;res',

// T
'texte_defaut' => 'Auteur de la photo (et lien) :',
'titre_module' => 'Photos myst&egrave;res',
'titre_mystere' => 'Photo myst&egrave;re des explorateurs',

// V
'voir_archives' => 'Consulter les archives',
'voir_solution' => 'D&eacute;voiler',


);

?>
