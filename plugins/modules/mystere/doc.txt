Mysteric
--------

Plugin pour jouer avec une photo mystère.

Licence: GPL3
Auteurs: marcimat,b_b

--------

*	Description

	Ce plugin permet de sélectionner un secteur qui contient des articles
	dont le logo est une photo mystérieuse dont on souhaite que les visiteurs
	trouvent ce que c'est.

	Chaque article contient un document image qui est la réponse
	(photo prise de plus loin).

	Le descriptif du document explique et donne plus de précision sur
	la réponse.

	Une réponse peut être affichée au bout d'un temps donné après la
	publication de l'article

	Les internautes peuvent commenter la photo mystère et leurs commentaires
	s'inscrivent dans le forum de l'article contenant la photo.

	
*	Notes

	Le texte de l'article n'est pas utilisé pour décrire la réponse,
	car il serait pris en compte trop facilement dans les recherches
	sur le site

	Dans l'interface privée, les documents des articles de cette rubrique
	doivent être cachés à tout rédacteur qui n'est pas auteur de l'article
	afin qu'aucune personne extérieurs au admins et à l'auteur ne
	puisse connaître la réponse


*	Modèles

	Un modèle permettant d'afficher la dernière photo mystère ou
	une photo mystère au hasard est proposé


*	Soumission d'une photo mystère

	Un formulaire pour soumettre une photo est disponible
	L'article créé est alors proposé à publication dans la rubrique
	de la photo mystère
